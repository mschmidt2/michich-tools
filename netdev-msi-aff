#!/bin/bash
# SPDX-License-Identifier: MIT
# SPDX-FileCopyrightText: Copyright Red Hat
# Author: Michal Schmidt

shopt -s nullglob

list_netdev_msi_irqs() {
	while (( $# )); do
		netdev="$1"
		for i in /sys/class/net/$netdev/device/msi_irqs/*; do
			irq=${i##*/}
			[ ! -d /proc/irq/"$irq" ] && continue
			smpa=-
			hint=-
			effa=-
			name=-
			read smpa < /proc/irq/"$irq"/smp_affinity
			read hint < /proc/irq/"$irq"/affinity_hint
			read effa < /proc/irq/"$irq"/effective_affinity
			name=$(find /proc/irq/"$irq" -mindepth 1 -maxdepth 1 -type d -printf '%f')
			echo "$netdev $irq $smpa $hint $effa $name"
		done
		shift
	done
}

{
	echo 'netdev IRQ# smp_affinity affinity_hint effective_affinity name'
	if (( $# )); then
		list_netdev_msi_irqs "$@"
	else
		cd /sys/class/net
		list_netdev_msi_irqs *
	fi
} | column -t
