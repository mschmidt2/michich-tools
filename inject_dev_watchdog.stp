#!/usr/bin/stap -vg
// Author: Michal Schmidt, Red Hat
// SPDX-License-Identifier: MIT
//
// Usage:
//   inject_dev_watchdog.stp DEVICE
//
// Tested on RHEL 8.6, 9.0, 9.2, 9.4.

probe begin {
	printf("Waiting for the periodic dev_watchdog timer to run for %s...\n", @1);
}

probe kernel.function("dev_watchdog") {
	dev = & @container_of($t, "struct net_device", watchdog_timer)
	name = kernel_string(dev->name);
	if (name == @1) {
		txq = &dev->_tx[0];
		printf("Injecting netdev watchdog on %s\n", name);
		printk(3, "inject_dev_watchdog.stp: Injecting an artificial netdev watchdog timeout\n");
		// set the QUEUE_STATE_DRV_XOFF bit
		txq->state = txq->state | 0x1;
		txq->trans_start = jiffies() - dev->watchdog_timeo - 1;
		exit()
	}
}
