// SPDX-License-Identifier: GPL-2.0 OR MIT
//
// Build the module (you need kernel-devel):
//   make
// Insert it. Replace eth0 with the appropriate netdev name:
//   insmod inject_dev_watchdog.ko netdev=eth0
// Wait for the watchdog to trigger. Usually up to 5 seconds for most drivers,
// but there are drivers (e.g. octeontx2) that set their watchdog_timeo as high
// as 100 seconds.
// The timeout is injected only once. To inject again, you have to:
//   rmmod inject_dev_watchdog
// and insmod again.

#include <linux/kernel.h>
#include <linux/kprobes.h>
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/netdevice.h>
#include <linux/sched.h>
#include <linux/timer.h>

static char *netdev;
module_param(netdev, charp, 0644);
MODULE_PARM_DESC(netdev, "Name of net device to inject watchdog timeout to");

static bool done;

static int dev_watchdog_pre(struct kprobe *p, struct pt_regs *regs)
{
	struct timer_list *t;
	struct net_device *dev;
	struct netdev_queue *txq;

	if (done)
		return 0;

	/* The argument to dev_watchdog is "struct timer_list *t" */
	/* first parameter starting from zero */
	t = (struct timer_list *)regs_get_kernel_argument(regs, 0);

	dev = from_timer(dev, t, watchdog_timer);

	if (netdev && strcmp(netdev, dev->name))
		return 0;

	printk(KERN_ERR "Injecting an artificial tx watchdog timeout for %s\n",
			dev->name);

	txq = netdev_get_tx_queue(dev, 0);
	netif_tx_stop_queue(txq);
	txq->trans_start = jiffies - dev->watchdog_timeo - 1;

	done = true;
	return 0;
}

/* kprobe structure */
static struct kprobe inject_watchdog = {
	.symbol_name = "dev_watchdog",
	.pre_handler = dev_watchdog_pre,
};

module_driver(inject_watchdog, register_kprobe, unregister_kprobe);
MODULE_LICENSE("Dual MIT/GPL");
MODULE_AUTHOR("Michal Schmidt"); // with help from ChatGPT
MODULE_DESCRIPTION("Inject a netdev watchdog timeout");
